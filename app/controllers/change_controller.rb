class ChangeController < ApplicationController

  def change_status
    @task = Task.find(params[:id])
    helpers.change_complete(@task)
    redirect_to list_path(@task.list_id), success: "Task successfully update"
  end

end