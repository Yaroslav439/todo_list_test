class ListsController < ApplicationController
  before_action :set_list, only: [:show, :edit, :update, :destroy]
    
  def new
    @list = List.new
    @list.tasks.build
  end
  
  def index
    @lists = List.all.order(created_at: :desc)
  end
  
  def show
  end
  
  def edit
  end
  
  def create
    @list = List.new(list_params)
    if @list.save
      redirect_to @list, success: "List successfully create"
    else
      render 'new', danger: "List not updated"
    end
  end
  
  def update
    if @list.update(list_params)
      redirect_to @list, success: "List successfully update"
    else
      render 'edit', danger: "List not updated"
    end
  end
  
  def destroy
    @list.destroy
    redirect_to lists_path, danger: "List successfully delete"
  end
  
  private
  
  def list_params
    params.require(:list).permit(:name, tasks_attributes: [:id, :title, :complete, :_destroy])
  end

  def set_list
    @list = List.find(params[:id])
  end
      
end
