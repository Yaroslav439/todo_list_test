class TasksController < ApplicationController
  before_action :set_task, only: [:show, :edit, :update, :destroy]

  def new
    @task = Task.new
    @list = List.find(params[:list_id])
  end
  
  def show
    @list = List.find(@task.list_id)
  end
  
  def edit
  end
  
  def create
    @task = Task.new(task_params)
    if @task.save
      redirect_to @task, success: "Task successfully create"
    else
      render 'new', danger: "Task not updated"
    end
  end
  
  def update
    if @task.update(task_params)
      redirect_to @task, success: "Task successfully update"
    else
      render 'edit', danger: "Task not updated"
    end
  end
  
  def destroy
    @list = List.find(@task.list_id)
    @task.destroy
    redirect_to list_path(@list), danger: "Task successfully delete"
  end
  
  private
  
  def task_params
    params.require(:task).permit(:title, :description, :complete, :list_id)
  end

  def set_task
    @task = Task.find(params[:id])
  end    
end
