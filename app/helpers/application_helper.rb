module ApplicationHelper
    def change_complete(task)
      if task.complete
        task.update_attributes(complete: false)
      else
        task.update_attributes(complete: true)
      end
    end
end
