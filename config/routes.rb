Rails.application.routes.draw do
  root 'lists#index'
  resources :lists
  resources :tasks, except: [:index]
  get 'change/change_status'
end
