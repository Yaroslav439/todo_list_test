10.times { List.create(name: Faker::Job.field) }
40.times { Task.create(title: Faker::Verb.base, description: Faker::Food.dish, list_id: List.all.ids.sample) }
40.times { Task.create(title: Faker::Verb.base, description: Faker::Food.dish, complete: true, list_id: List.all.ids.sample) }
